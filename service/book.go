package service

import (
	"context"
	"fmt"
	"gitlab.com/booklog/booklog/rpc/v1/bookservice"
	"gitlab.com/booklog/booklogweb/config"
	"google.golang.org/grpc"
)

// Book is the book service interface
type Book interface {
	BookList() (*bookservice.BooksReply, error)
	BookLog(book string) (*bookservice.BookLogReply, error)
	BookLogUpdate(action, book string, chapter int) (*bookservice.BookLogUpdateReply, error)
}

// BookService implements the book service
type BookService struct {
	Settings *config.Settings
	Client   bookservice.BookClient
}

// NewBook returns a new identity service
func NewBook(settings *config.Settings, client bookservice.BookClient) *BookService {
	return &BookService{
		Settings: settings,
		Client:   client,
	}
}

// BookConnect connects to the backend book service
func BookConnect(settings *config.Settings) (*grpc.ClientConn, func(), error) {
	// Make the gRPC request to the backend
	url := fmt.Sprintf("%s%s", settings.BookServiceDomain, settings.BookServicePort)
	conn, err := grpc.Dial(url, grpc.WithInsecure())
	if err != nil {
		err = fmt.Errorf("fail to dial: %v", err)
	}

	return conn, func() {
		conn.Close()
	}, err
}

// BookList lists the books
func (srv BookService) BookList() (*bookservice.BooksReply, error) {
	// Login to the backend service
	req := bookservice.BooksRequest{}
	u, err := srv.Client.BookList(context.Background(), &req)
	return u, err
}

// BookLog fetches the log for a book
func (srv BookService) BookLog(book string) (*bookservice.BookLogReply, error) {
	// Login to the backend service
	req := bookservice.BookLogRequest{
		Code: book,
	}
	u, err := srv.Client.BookLog(context.Background(), &req)
	return u, err
}

// BookLogUpdate updates a reading log for a book
func (srv BookService) BookLogUpdate(action, book string, chapter int) (*bookservice.BookLogUpdateReply, error) {
	var act bookservice.BookLogUpdateRequest_Action

	// Convert action to the enumeration
	switch action {
	case "add":
		act = bookservice.BookLogUpdateRequest_ADD
	case "remove":
		act = bookservice.BookLogUpdateRequest_REMOVE
	default:
		return &bookservice.BookLogUpdateReply{}, fmt.Errorf("action must be 'add' or 'remove'")
	}

	// Login to the backend service
	req := bookservice.BookLogUpdateRequest{
		Action:  act,
		Code:    book,
		Chapter: int32(chapter),
	}
	u, err := srv.Client.BookLogUpdate(context.Background(), &req)
	return u, err
}
