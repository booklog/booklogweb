package service

import (
	"context"
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/booklog/booklog/rpc/v1/bookservice"
	"gitlab.com/booklog/booklogweb/config"
	"google.golang.org/grpc"
)

type mockBook struct{}

func (m *mockBook) BookList(ctx context.Context, in *bookservice.BooksRequest, opts ...grpc.CallOption) (*bookservice.BooksReply, error) {
	b1 := bookservice.BookInfo{Code: "gen", Name: "Genesis", Chapters: 50}
	b2 := bookservice.BookInfo{Code: "ruth", Name: "Ruth", Chapters: 4}
	return &bookservice.BooksReply{Books: []*bookservice.BookInfo{&b1, &b2}}, nil
}

func (m *mockBook) BookLogUpdate(ctx context.Context, in *bookservice.BookLogUpdateRequest, opts ...grpc.CallOption) (*bookservice.BookLogUpdateReply, error) {
	if in.Code == "ruth" {
		return &bookservice.BookLogUpdateReply{}, nil
	}
	return nil, fmt.Errorf("MOCK book log update error")
}

func (m *mockBook) BookLog(ctx context.Context, in *bookservice.BookLogRequest, opts ...grpc.CallOption) (*bookservice.BookLogReply, error) {
	b := bookservice.BookInfo{Code: "ruth", Name: "Ruth", Chapters: 4}
	return &bookservice.BookLogReply{Book: &b, Read: []int32{1, 0, 5, 4}}, nil
}

func TestNewBook(t *testing.T) {
	settings := config.ParseArgs()
	reply := BookService{
		Settings: settings,
		Client:   &mockBook{},
	}

	type args struct {
		settings *config.Settings
		client   bookservice.BookClient
	}
	tests := []struct {
		name string
		args args
		want *BookService
	}{
		{"create", args{settings, &mockBook{}}, &reply},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewBook(tt.args.settings, tt.args.client); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewBook() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBookService_BookList(t *testing.T) {
	settings := config.ParseArgs()
	reply1 := &bookservice.BooksReply{Books: []*bookservice.BookInfo{
		{Code: "gen", Name: "Genesis", Chapters: 50},
		{Code: "ruth", Name: "Ruth", Chapters: 4},
	}}

	type fields struct {
		Settings *config.Settings
		Client   bookservice.BookClient
	}
	tests := []struct {
		name    string
		fields  fields
		want    *bookservice.BooksReply
		wantErr bool
	}{
		{"success", fields{settings, &mockBook{}}, reply1, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := BookService{
				Settings: tt.fields.Settings,
				Client:   tt.fields.Client,
			}
			got, err := srv.BookList()
			if (err != nil) != tt.wantErr {
				t.Errorf("BookService.BookList() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("BookService.BookList() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBookService_BookLog(t *testing.T) {
	settings := config.ParseArgs()
	reply1 := &bookservice.BookLogReply{
		Book: &bookservice.BookInfo{Code: "ruth", Name: "Ruth", Chapters: 4},
		Read: []int32{1, 0, 5, 4},
	}

	type fields struct {
		Settings *config.Settings
		Client   bookservice.BookClient
	}
	type args struct {
		book string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *bookservice.BookLogReply
		wantErr bool
	}{
		{"ruth", fields{settings, &mockBook{}}, args{"ruth"}, reply1, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := BookService{
				Settings: tt.fields.Settings,
				Client:   tt.fields.Client,
			}
			got, err := srv.BookLog(tt.args.book)
			if (err != nil) != tt.wantErr {
				t.Errorf("BookService.BookLog() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("BookService.BookLog() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBookService_BookLogUpdate(t *testing.T) {
	settings := config.ParseArgs()
	reply1 := &bookservice.BookLogUpdateReply{}

	type fields struct {
		Settings *config.Settings
		Client   bookservice.BookClient
	}
	type args struct {
		action  string
		book    string
		chapter int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *bookservice.BookLogUpdateReply
		wantErr bool
	}{
		{"ruth-add", fields{settings, &mockBook{}}, args{"add", "ruth", 1}, reply1, false},
		{"ruth-remove", fields{settings, &mockBook{}}, args{"remove", "ruth", 1}, reply1, false},
		{"ruth-invalid", fields{settings, &mockBook{}}, args{"invalid", "ruth", 1}, reply1, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := BookService{
				Settings: tt.fields.Settings,
				Client:   tt.fields.Client,
			}
			got, err := srv.BookLogUpdate(tt.args.action, tt.args.book, tt.args.chapter)
			if (err != nil) != tt.wantErr {
				t.Errorf("BookService.BookLogUpdate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("BookService.BookLogUpdate() = %v, want %v", got, tt.want)
			}
		})
	}
}
