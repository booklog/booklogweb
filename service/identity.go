package service

import (
	"context"
	"fmt"
	"gitlab.com/booklog/booklogweb/config"
	"gitlab.com/booklog/identity/app/rpc/v1/proto/userservice"
	"google.golang.org/grpc"
)

// Identity is the interface for the identity service
type Identity interface {
	Login(user, password string) (*userservice.LoginReply, error)
}

// IdentityService implements the identity service
type IdentityService struct {
	Settings *config.Settings
	Client   userservice.UserClient
}

// NewIdentity returns a new identity service
func NewIdentity(settings *config.Settings, client userservice.UserClient) *IdentityService {
	return &IdentityService{
		Settings: settings,
		Client:   client,
	}
}

// IdentityConnect is the gRPC connection to the backend identity service
func IdentityConnect(settings *config.Settings) (*grpc.ClientConn, func(), error) {
	// Make the gRPC request to the backend
	url := fmt.Sprintf("%s%s", settings.IdentityServiceDomain, settings.IdentityServicePort)
	conn, err := grpc.Dial(url, grpc.WithInsecure())
	if err != nil {
		err = fmt.Errorf("fail to dial: %v", err)
	}

	return conn, func() {
		conn.Close()
	}, err
}

// Login logs into the identity service
func (srv IdentityService) Login(user, password string) (*userservice.LoginReply, error) {
	// Login to the backend service
	req := userservice.LoginRequest{User: user, Password: password}
	u, err := srv.Client.Login(context.Background(), &req)
	return u, err
}
