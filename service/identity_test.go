package service

import (
	"context"
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/booklog/booklogweb/config"
	"gitlab.com/booklog/identity/app/rpc/v1/proto/userservice"
	"google.golang.org/grpc"
)

type mockUserClient struct{}

func (m *mockUserClient) Login(ctx context.Context, in *userservice.LoginRequest, opts ...grpc.CallOption) (*userservice.LoginReply, error) {
	if in.User == "john" && in.Password == "secret" {
		return &userservice.LoginReply{User: &userservice.UserInfo{User: "john", Name: "John Doe", Email: "john@example.com", Role: userservice.UserInfo_STANDARD}}, nil
	}
	return nil, fmt.Errorf("MOCK login error")
}

func (m *mockUserClient) Verify(ctx context.Context, in *userservice.VerifyRequest, opts ...grpc.CallOption) (*userservice.VerifyReply, error) {
	panic("implement me")
}

func TestNewIdentity(t *testing.T) {
	settings := config.ParseArgs()
	reply := IdentityService{
		Settings: settings,
		Client:   &mockUserClient{},
	}

	type args struct {
		settings *config.Settings
	}
	tests := []struct {
		name string
		args args
		want *IdentityService
	}{
		{"create", args{settings}, &reply},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewIdentity(tt.args.settings, &mockUserClient{}); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewIdentity() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIdentityService_Login(t *testing.T) {
	settings := config.ParseArgs()
	reply1 := &userservice.LoginReply{User: &userservice.UserInfo{User: "john", Name: "John Doe", Email: "john@example.com", Role: userservice.UserInfo_STANDARD}}

	type fields struct {
		Settings *config.Settings
	}
	type args struct {
		user     string
		password string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *userservice.LoginReply
		wantErr bool
	}{
		{"success", fields{settings}, args{"john", "secret"}, reply1, false},
		{"bad-user", fields{settings}, args{"unknown", "secret"}, nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := IdentityService{
				Settings: tt.fields.Settings,
				Client:   &mockUserClient{},
			}
			got, err := srv.Login(tt.args.user, tt.args.password)
			if (err != nil) != tt.wantErr {
				t.Errorf("IdentityService.Login() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("IdentityService.Login() = %v, want %v", got, tt.want)
			}
		})
	}
}
