package web

import (
	"encoding/json"
	"gitlab.com/booklog/router/response"
	"google.golang.org/grpc/status"
	"io"
	"log"
	"net/http"
	"time"
)

// JWTCookie is the name of the cookie used to store the JWT
const JWTCookie = "X-Auth-Token"

type loginRequest struct {
	User     string `json:"user"`
	Password string `json:"password"`
}

// Login sends a gRPC login request to the backend
func (srv Controller) Login(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", response.JSONHeader)

	// Decode the JSON body
	user, err := decodeLoginRequest(w, r)
	if err != nil {
		return
	}

	// Login to the backend service (gRPC)
	reply, err := srv.Identity.Login(user.User, user.Password)
	if err != nil {
		st, _ := status.FromError(err)
		log.Printf("Error with login: (%s) %s", st.Code(), st.Message())
		response.FormatStandardResponse(false, st.Code().String(), st.Message(), w)
		return
	}

	// Create the JWT cookie
	addJWTCookie(reply.Token, w)

	response.FormatStandardResponse(true, "", "", w)
}

func decodeLoginRequest(w http.ResponseWriter, r *http.Request) (loginRequest, error) { // Decode the REST request
	defer r.Body.Close()

	// Decode the JSON body
	user := loginRequest{}
	err := json.NewDecoder(r.Body).Decode(&user)
	switch {
	// Check we have some data
	case err == io.EOF:
		response.FormatStandardResponse(false, "NoData", "No data supplied.", w)
		log.Println("No data supplied.")
		// Check for parsing errors
	case err != nil:
		response.FormatStandardResponse(false, "BadData", err.Error(), w)
		log.Println(err)
	}
	return user, err
}

// addJWTCookie sets the JWT as a cookie
func addJWTCookie(jwtToken string, w http.ResponseWriter) {
	// Set the JWT as a bearer token
	// (In practice, the cookie will be used more as clicking on a page link will not send the auth header)
	w.Header().Set("Authorization", "Bearer "+jwtToken)

	expireCookie := time.Now().Add(time.Hour * 1)
	cookie := http.Cookie{Name: JWTCookie, Value: jwtToken, Expires: expireCookie, HttpOnly: true}
	http.SetCookie(w, &cookie)
}
