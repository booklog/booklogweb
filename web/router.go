package web

import (
	"gitlab.com/booklog/router/middleware"
	"net/http"

	"github.com/gorilla/mux"
)

// Router returns the application router
func (srv Controller) Router() *mux.Router {
	// Start the web service router
	router := mux.NewRouter()

	router.Handle("/api/login", middleware.Middleware(http.HandlerFunc(srv.Login))).Methods("POST")
	router.Handle("/api/books", middleware.Middleware(http.HandlerFunc(srv.BookList))).Methods("GET")
	router.Handle("/api/booklog/{book}", middleware.Middleware(http.HandlerFunc(srv.BookLog))).Methods("GET")
	router.Handle("/api/booklog/{book}/{chapter:[0-9]+}/{action}", middleware.Middleware(http.HandlerFunc(srv.BookLogUpdate))).Methods("POST")

	// Serve the static path
	fs := http.StripPrefix("/static/", http.FileServer(http.Dir(srv.Settings.DocRoot+"/static")))
	router.PathPrefix("/static/").Handler(fs)

	// Default path is the index page
	router.Handle("/", middleware.Middleware(http.HandlerFunc(srv.Index))).Methods("GET")
	router.NotFoundHandler = middleware.Middleware(http.HandlerFunc(srv.Index))

	return router
}
