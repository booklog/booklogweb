package web

import (
	"fmt"
	"gitlab.com/booklog/booklogweb/service"
	"net/http"

	"gitlab.com/booklog/booklogweb/config"
)

// Controller is the API web service
type Controller struct {
	Settings *config.Settings
	Identity service.Identity
	Book     service.Book
}

// NewServer returns a new web controller
func NewServer(settings *config.Settings, id service.Identity, book service.Book) *Controller {
	ctrl := Controller{
		Settings: settings,
		Identity: id,
		Book:     book,
	}

	return &ctrl
}

// Run starts the web server
func (srv Controller) Run() error {
	fmt.Printf("Starting service on port %s\n", srv.Settings.Port)
	return http.ListenAndServe(srv.Settings.Port, srv.Router())
}
