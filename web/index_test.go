package web

import (
	"github.com/stretchr/testify/assert"
	"testing"

	"gitlab.com/booklog/booklogweb/config"
)

func TestController_Index(t *testing.T) {
	settings1 := config.ParseArgs()
	settings1.IndexTemplate = "../../testdata/index.html"
	settings2 := config.ParseArgs()
	settings2.IndexTemplate = "/static/does_not_exist.html"

	type fields struct {
		Settings *config.Settings
		code     int
	}
	type args struct {
		method string
		url    string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{"valid", fields{settings1, 200}, args{"GET", "/"}},
		{"does-not-exist", fields{settings2, 500}, args{"GET", "/"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := Controller{
				Settings: tt.fields.Settings,
				Identity: &mockIdentity{},
				Book:     &mockBook{},
			}
			w := sendRequest(tt.args.method, tt.args.url, nil, &srv)
			assert.Equal(t, tt.fields.code, w.Code)
		})
	}
}
