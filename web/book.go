package web

import (
	"github.com/gorilla/mux"
	"gitlab.com/booklog/booklogweb/domain"
	"gitlab.com/booklog/router/response"
	"google.golang.org/grpc/status"
	"log"
	"net/http"
	"strconv"
)

// BookList sends a gRPC book list request to the backend
func (srv Controller) BookList(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", response.JSONHeader)

	reply, err := srv.Book.BookList()
	if err != nil {
		st, _ := status.FromError(err)
		log.Printf("Error fetching books: (%s) %s", st.Code(), st.Message())
		_ = response.FormatStandardResponse(false, st.Code().String(), st.Message(), w)
		return
	}

	// Convert the reply to a JSON list response
	books := make([]domain.Book, len(reply.Books))
	for i, b := range reply.Books {
		book := domain.Book{
			Code:     b.Code,
			Name:     b.Name,
			Chapters: int(b.Chapters),
		}
		books[i] = book
	}
	_ = response.FormatListResponse(books, w)
}

// BookLog sends a gRPC request to get the book log from the backend
func (srv Controller) BookLog(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", response.JSONHeader)
	vars := mux.Vars(r)

	reply, err := srv.Book.BookLog(vars["book"])
	if err != nil {
		st, _ := status.FromError(err)
		log.Printf("Error fetching book log: (%s) %s", st.Code(), st.Message())
		_ = response.FormatStandardResponse(false, st.Code().String(), st.Message(), w)
		return
	}

	bl := domain.BookLog{
		Book: reply.Book.Code,
		Name: reply.Book.Name,
		Read: make([]int, len(reply.Read)),
	}
	for i, r := range reply.Read {
		bl.Read[i] = int(r)
	}

	_ = response.FormatListResponse(bl, w)
}

// BookLogUpdate sends a gRPC update to the reading log
func (srv Controller) BookLogUpdate(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", response.JSONHeader)
	vars := mux.Vars(r)

	chapter, _ := strconv.Atoi(vars["chapter"])

	_, err := srv.Book.BookLogUpdate(vars["action"], vars["book"], chapter)
	if err != nil {
		st, _ := status.FromError(err)
		log.Printf("Error updating book log: (%s) %s", st.Code(), st.Message())
		_ = response.FormatStandardResponse(false, st.Code().String(), st.Message(), w)
		return
	}
	_ = response.FormatStandardResponse(true, "", "", w)
}
