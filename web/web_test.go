package web

import (
	"fmt"
	"gitlab.com/booklog/booklog/rpc/v1/bookservice"
	"gitlab.com/booklog/identity/app/rpc/v1/proto/userservice"
	"google.golang.org/grpc"
	"reflect"
	"testing"

	"gitlab.com/booklog/booklogweb/config"
)

type mockBook struct{}

func (m mockBook) BookList() (*bookservice.BooksReply, error) {
	b1 := bookservice.BookInfo{Code: "gen", Name: "Genesis", Chapters: 50}
	b2 := bookservice.BookInfo{Code: "ruth", Name: "Ruth", Chapters: 4}
	return &bookservice.BooksReply{Books: []*bookservice.BookInfo{&b1, &b2}}, nil
}

func (m mockBook) BookLog(book string) (*bookservice.BookLogReply, error) {
	b := bookservice.BookInfo{Code: "ruth", Name: "Ruth", Chapters: 4}
	return &bookservice.BookLogReply{Book: &b, Read: []int32{1, 0, 5, 4}}, nil
}

func (m mockBook) BookLogUpdate(action, book string, chapter int) (*bookservice.BookLogUpdateReply, error) {
	if book == "ruth" {
		return nil, nil
	}
	return nil, fmt.Errorf("MOCK book log update error")
}

type mockBookErr struct{}

func (m mockBookErr) Connect() (*grpc.ClientConn, func(), error) {
	return nil, nil, fmt.Errorf("MOCK connect error")
}

func (m mockBookErr) BookList() (*bookservice.BooksReply, error) {
	return &bookservice.BooksReply{}, fmt.Errorf("MOCK book list error")
}

func (m mockBookErr) BookLog(book string) (*bookservice.BookLogReply, error) {
	return nil, fmt.Errorf("MOCK book log error")
}

func (m mockBookErr) BookLogUpdate(action, book string, chapter int) (*bookservice.BookLogUpdateReply, error) {
	return nil, fmt.Errorf("MOCK update error")
}

type mockIdentity struct{}

func (srv mockIdentity) Connect() (*grpc.ClientConn, func(), error) {
	panic("implement me")
}

func (srv mockIdentity) Login(user, password string) (*userservice.LoginReply, error) {
	if user == "john" && password == "secret" {
		return &userservice.LoginReply{User: &userservice.UserInfo{User: "john", Name: "John Doe", Email: "john@example.com", Role: userservice.UserInfo_STANDARD}}, nil
	}
	return &userservice.LoginReply{}, fmt.Errorf("MOCK login error")
}

func TestNewServer(t *testing.T) {
	settings := &config.Settings{}
	reply := &Controller{settings, &mockIdentity{}, &mockBook{}}

	type args struct {
		settings *config.Settings
	}
	tests := []struct {
		name string
		args args
		want *Controller
	}{
		{"new-server", args{settings: settings}, reply},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewServer(tt.args.settings, &mockIdentity{}, &mockBook{}); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewServer() = %v, want %v", got, tt.want)
			}
		})
	}
}
