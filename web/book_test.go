package web

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"log"
	"testing"

	"gitlab.com/booklog/booklogweb/config"
	"gitlab.com/booklog/booklogweb/service"
)

func TestController_BookList(t *testing.T) {
	settings := config.ParseArgs()
	reply1 := `{"success":true,"code":"","message":"","records":[{"code":"gen","name":"Genesis","chapters":50},{"code":"ruth","name":"Ruth","chapters":4}]}` + "\n"
	reply2 := `{"success":false,"code":"Unknown","message":"MOCK book list error"}` + "\n"

	type fields struct {
		Settings *config.Settings
		Book     service.Book
	}
	tests := []struct {
		name   string
		fields fields
		code   int
		reply  string
	}{
		{"valid", fields{settings, &mockBook{}}, 200, reply1},
		{"error", fields{settings, &mockBookErr{}}, 400, reply2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := Controller{
				Settings: tt.fields.Settings,
				Identity: mockIdentity{},
				Book:     tt.fields.Book,
			}
			w := sendRequest("GET", "/api/books", nil, &srv)
			assert.Equal(t, tt.code, w.Code)
			assert.Equal(t, tt.reply, fmt.Sprint(w.Body))
		})
	}
}

func TestController_BookLog(t *testing.T) {
	settings := config.ParseArgs()
	reply1 := `{"success":true,"code":"","message":"","records":{"book":"ruth","name":"Ruth","read":[1,0,5,4]}}` + "\n"
	reply2 := `{"success":false,"code":"Unknown","message":"MOCK book log error"}` + "\n"

	type fields struct {
		Settings *config.Settings
		Book     service.Book
	}
	tests := []struct {
		name   string
		fields fields
		code   int
		reply  string
	}{
		{"valid", fields{settings, &mockBook{}}, 200, reply1},
		{"invalid", fields{settings, &mockBookErr{}}, 400, reply2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := Controller{
				Settings: tt.fields.Settings,
				Identity: &mockIdentity{},
				Book:     tt.fields.Book,
			}
			w := sendRequest("GET", "/api/booklog/ruth", nil, &srv)
			assert.Equal(t, tt.code, w.Code)
			assert.Equal(t, tt.reply, fmt.Sprint(w.Body))
		})
	}
}

func TestController_BookLogUpdate(t *testing.T) {
	settings := config.ParseArgs()
	reply1 := `{"success":true,"code":"","message":""}` + "\n"
	reply2 := `{"success":false,"code":"Unknown","message":"MOCK update error"}` + "\n"

	type fields struct {
		Settings *config.Settings
		Book     service.Book
	}
	tests := []struct {
		name   string
		fields fields
		action string
		code   int
		reply  string
	}{
		{"valid-add", fields{settings, &mockBook{}}, "add", 200, reply1},
		{"valid-remove", fields{settings, &mockBook{}}, "remove", 200, reply1},
		{"invalid-add", fields{settings, &mockBookErr{}}, "add", 400, reply2},
		{"invalid-remove", fields{settings, &mockBookErr{}}, "remove", 400, reply2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := Controller{
				Settings: tt.fields.Settings,
				Identity: &mockIdentity{},
				Book:     tt.fields.Book,
			}
			w := sendRequest("POST", "/api/booklog/ruth/1/"+tt.action, nil, &srv)
			assert.Equal(t, tt.code, w.Code)
			log.Println(w.Body)
			assert.Equal(t, tt.reply, fmt.Sprint(w.Body))
		})
	}
}
