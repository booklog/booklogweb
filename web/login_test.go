package web

import (
	"bytes"
	"fmt"
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/booklog/booklogweb/config"
)

func TestController_Login(t *testing.T) {
	// Setup dependencies
	settings := &config.Settings{}
	srv := NewServer(settings, &mockIdentity{}, &mockBook{})

	// Data
	data1 := []byte(`{"user":"john", "password":"secret"}`)
	reply1 := `{"success":true,"code":"","message":""}` + "\n"
	data2 := []byte("")
	reply2 := `{"success":false,"code":"NoData","message":"No data supplied."}` + "\n"
	data3 := []byte(`{"user":"unknown", "password":"invalid"}`)
	reply3 := `{"success":false,"code":"Unknown","message":"MOCK login error"}` + "\n"
	data4 := []byte(`\u100`)
	reply4 := `{"success":false,"code":"BadData","message":"invalid character '\\\\' looking for beginning of value"}` + "\n"

	type fields struct {
		Controller *Controller
		data       []byte
		reply      string
		code       int
	}
	tests := []struct {
		name   string
		fields fields
	}{
		{"success", fields{srv, data1, reply1, 200}},
		{"empty-data", fields{srv, data2, reply2, 400}},
		{"failure", fields{srv, data3, reply3, 400}},
		{"bad-data", fields{srv, data4, reply4, 400}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := sendRequest("POST", "/api/login", bytes.NewReader(tt.fields.data), srv)
			assert.Equal(t, tt.fields.code, w.Code)
			assert.Equal(t, tt.fields.reply, fmt.Sprint(w.Body))
			if w.Code == 200 {
				assert.True(t, len(w.Header().Get("Authorization")) > 0)
			} else {
				assert.True(t, len(w.Header().Get("Authorization")) == 0)
			}
		})
	}
}

func sendRequest(method, url string, data io.Reader, srv *Controller) *httptest.ResponseRecorder {
	w := httptest.NewRecorder()
	r, _ := http.NewRequest(method, url, data)

	srv.Router().ServeHTTP(w, r)

	return w
}
