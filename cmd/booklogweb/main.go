package main

import (
	"gitlab.com/booklog/booklog/rpc/v1/bookservice"
	"gitlab.com/booklog/booklogweb/service"
	"gitlab.com/booklog/identity/app/rpc/v1/proto/userservice"
	"log"

	"gitlab.com/booklog/booklogweb/config"
	"gitlab.com/booklog/booklogweb/web"
)

func main() {
	// Initialize the dependency chain
	settings := config.ParseArgs()

	// Set up the identity service client
	connIdentity, closeID, err := service.IdentityConnect(settings)
	defer closeID()
	if err != nil {
		log.Fatalf("Error connecting to identity service: %v", err)
	}
	clientIdentity := userservice.NewUserClient(connIdentity)
	srvIdentity := service.NewIdentity(settings, clientIdentity)

	// Set up the book service client
	connBook, closeBook, err := service.BookConnect(settings)
	defer closeBook()
	if err != nil {
		log.Fatalf("Error connecting to book service: %v", err)
	}
	clientBook := bookservice.NewBookClient(connBook)
	srvBook := service.NewBook(settings, clientBook)

	// Start the web router
	server := web.NewServer(settings, srvIdentity, srvBook)
	log.Fatal(server.Run())
}
