FROM golang:1.11 as builder1
COPY . ./src/gitlab.com/booklog/booklogweb
WORKDIR /go/src/gitlab.com/booklog/booklogweb
RUN ./get-deps.sh
RUN go test ./...
RUN CGO_ENABLED=0 GOOS=linux go build -a -o /go/bin/booklogweb -ldflags='-extldflags "-static"' cmd/booklogweb/main.go


FROM node:8-alpine as builder2
COPY webapp .
WORKDIR /
RUN npm run build

ARG PORT=":8000"
ARG APISCHEME=http
ARG APIDOMAIN=localhost
ARG APIPORT=":8010"

ENV PORT="${PORT}"
ENV APISCHEME="${APISCHEME}"
ENV APIDOMAIN="${APIDOMAIN}"
ENV APIPORT="${APIPORT}"

# Copy the built applications to the docker image
FROM alpine
WORKDIR /root/
RUN apk --no-cache add ca-certificates
COPY --from=builder1 /go/bin/booklogweb .
COPY --from=builder2 /build ./webapp/build
EXPOSE 8000
ENTRYPOINT ./booklogweb
