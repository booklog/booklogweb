package config

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestParseArgs(t *testing.T) {
	tests := []struct {
		name string
	}{
		{"default-settings"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := ParseArgs()
			assert.Equal(t, DefaultPort, got.Port, tt.name)
			assert.Equal(t, DefaultDocRoot, got.DocRoot, tt.name)
			assert.Equal(t, DefaultIndexTemplate, got.IndexTemplate, tt.name)
			assert.Equal(t, DefaultAPIDomain, got.APIDomain, tt.name)
			assert.Equal(t, DefaultAPIScheme, got.APIScheme, tt.name)
			assert.Equal(t, DefaultAPIPort, got.APIPort, tt.name)
			assert.Equal(t, DefaultBookDomain, got.BookServiceDomain, tt.name)
			assert.Equal(t, DefaultBookPort, got.BookServicePort, tt.name)
			assert.Equal(t, DefaultIdentityDomain, got.IdentityServiceDomain, tt.name)
			assert.Equal(t, DefaultIdentityPort, got.IdentityServicePort, tt.name)
		})
	}
}

func TestParseArgsEnv(t *testing.T) {
	tests := []struct {
		name string
	}{
		{"env-settings"},
	}
	for _, tt := range tests {
		os.Setenv("PORT", ":1234")
		os.Setenv("APIPORT", ":2345")
		os.Setenv("APIDOMAIN", "booklogweb")
		os.Setenv("APISCHEME", "ws")
		os.Setenv("BOOKDOMAIN", "booklog")
		os.Setenv("BOOKPORT", ":3456")
		os.Setenv("IDENTITYDOMAIN", "identity")
		os.Setenv("IDENTITYPORT", ":4567")

		t.Run(tt.name, func(t *testing.T) {
			got := ParseArgs()
			assert.Equal(t, ":1234", got.Port, tt.name)
			assert.Equal(t, DefaultDocRoot, got.DocRoot, tt.name)
			assert.Equal(t, DefaultIndexTemplate, got.IndexTemplate, tt.name)
			assert.Equal(t, "booklogweb", got.APIDomain, tt.name)
			assert.Equal(t, "ws", got.APIScheme, tt.name)
			assert.Equal(t, ":2345", got.APIPort, tt.name)
			assert.Equal(t, "booklog", got.BookServiceDomain, tt.name)
			assert.Equal(t, ":3456", got.BookServicePort, tt.name)
			assert.Equal(t, "identity", got.IdentityServiceDomain, tt.name)
			assert.Equal(t, ":4567", got.IdentityServicePort, tt.name)
		})
	}
}
