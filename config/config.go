package config

import "os"

// Default settings
const (
	DefaultPort           = ":8000"
	DefaultDocRoot        = "webapp/build"
	DefaultIndexTemplate  = "index.html"
	DefaultAPIPort        = ":8010"
	DefaultAPIDomain      = "localhost"
	DefaultAPIScheme      = "http"
	DefaultBookPort       = ":8010"
	DefaultBookDomain     = "localhost"
	DefaultIdentityPort   = ":8020"
	DefaultIdentityDomain = "localhost"
)

// Settings defines the application configuration
type Settings struct {
	Port                  string
	DocRoot               string
	IndexTemplate         string
	APIScheme             string
	APIDomain             string
	APIPort               string
	BookServiceDomain     string
	BookServicePort       string
	IdentityServiceDomain string
	IdentityServicePort   string
}

// ParseArgs checks the environment variables
func ParseArgs() *Settings {
	var (
		port           = DefaultPort
		apiPort        = DefaultAPIPort
		apiDomain      = DefaultAPIDomain
		apiScheme      = DefaultAPIScheme
		bookDomain     = DefaultBookDomain
		bookPort       = DefaultBookPort
		identityDomain = DefaultIdentityDomain
		identityPort   = DefaultIdentityPort
	)

	if len(os.Getenv("PORT")) > 0 {
		port = os.Getenv("PORT")
	}
	if len(os.Getenv("APIPORT")) > 0 {
		apiPort = os.Getenv("APIPORT")
	}
	if len(os.Getenv("APIDOMAIN")) > 0 {
		apiDomain = os.Getenv("APIDOMAIN")
	}
	if len(os.Getenv("APISCHEME")) > 0 {
		apiScheme = os.Getenv("APISCHEME")
	}
	if len(os.Getenv("BOOKDOMAIN")) > 0 {
		bookDomain = os.Getenv("BOOKDOMAIN")
	}
	if len(os.Getenv("BOOKPORT")) > 0 {
		bookPort = os.Getenv("BOOKPORT")
	}
	if len(os.Getenv("IDENTITYDOMAIN")) > 0 {
		identityDomain = os.Getenv("IDENTITYDOMAIN")
	}
	if len(os.Getenv("IDENTITYPORT")) > 0 {
		identityPort = os.Getenv("IDENTITYPORT")
	}

	return &Settings{
		Port:                  port,
		DocRoot:               DefaultDocRoot,
		IndexTemplate:         DefaultIndexTemplate,
		APIScheme:             apiScheme,
		APIDomain:             apiDomain,
		APIPort:               apiPort,
		BookServiceDomain:     bookDomain,
		BookServicePort:       bookPort,
		IdentityServiceDomain: identityDomain,
		IdentityServicePort:   identityPort,
	}
}
