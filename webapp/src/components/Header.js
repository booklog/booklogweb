import React, { Component } from 'react'

export default class Header extends Component {
  render() {
    return (
      <header>
        <h1>Book Log</h1>
      </header>
    )
  }
}
