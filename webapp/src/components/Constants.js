const API_PREFIX = '/api/'

function getBaseURL() {
    return window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + API_PREFIX;
}

var Constants = {
    baseUrl: getBaseURL()
}

export default Constants
