import React from 'react'
import API from './API'

class Login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: '',
            password: '',
        };
    }

    handleChangeUser = (e) => {
        this.setState({user: e.target.value});
    }

    handleChangePassword = (e) => {
        this.setState({password: e.target.value});
    }

    handleLogin = (e) => {
        e.preventDefault();

        API.login(this.state.user, this.state.password).then(response => {
            console.log('---', response.data)
            if (response.data.success) {
                window.location.pathname = "/chart"
            }
        })
    }

    render() {
        return (
            <div className="block">
                <h3>Sign-in</h3>
                <form>
                    <label htmlFor="user">Username:
                        <input type="text" id="user" placeholder="Enter your username" className="form-control"
                               value={this.state.user} onChange={this.handleChangeUser} />
                    </label>
                    <label htmlFor="password">Password:
                        <input type="password" id="password" placeholder="Enter your password" className="form-control"
                               value={this.state.password} onChange={this.handleChangePassword} />
                    </label>
                </form>

                <div>
                    <button className="primary" onClick={this.handleLogin}>Login</button>
                </div>
            </div>
        );
    }
}

Login.propTypes = {};

export default Login;