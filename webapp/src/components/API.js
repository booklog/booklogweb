import axios from 'axios'
import Constants from './Constants'

var service = {
    login: (user, password) => {
        return axios.post(Constants.baseUrl + 'login', {user:user, password: password});
    },

    bookList: () => {
        return axios.get(Constants.baseUrl + 'books');
    },

    bookLog: (b) => {
        return axios.get(Constants.baseUrl + 'booklog/' + b);
    },

    bookLogUpdate: (action, b, ch) => {
        return axios.post(Constants.baseUrl + 'booklog/' + b + '/' + ch + '/' + action);
    }
}

export default service