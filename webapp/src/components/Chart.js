import React, { Component } from 'react'
import chroma from 'chroma-js'

const colors = chroma.scale(['#B5D8EF','#0D4875']).mode('lch').colors(20);

export default class Chart extends Component {

  constructor(props) {
    super(props);
    this.state = {
      chapters: 0
    };
  }

  handleChange = (e) => {
    this.props.onChange(e.target.value)
  }

  handleClick = (e) => {
    this.props.onClick(e.target.getAttribute('data-key'))
  }

  renderChart() {
    var rows = []
      for(var i=0; i<this.props.read.length; i++) {
        var button = <button className="chartbox" key={i+1} data-key={i+1} onClick={this.handleClick}>{i+1}</button>
        if (this.props.read[i]>0) {
          let j = this.props.read[i];
          if (j > 19) {j = 19}
          button = <button className="chartbox selected" style={{backgroundColor: colors[j]}} title={this.props.read[i] + ' reads'}
                           key={i+1} data-key={i+1} onClick={this.handleClick}>{i+1}</button>
        }
        rows.push(
          button
        )
      }
    return <div>{rows}</div>
  }

  renderSelect() {
    return (
        <div className="select-wrapper">
          <select className="select" value={this.props.selectedBook} onChange={this.handleChange}>
            {this.props.books.map(o => {
              return <option value={o.code}>{o.name}</option>
            })}
          </select>
        </div>
    )
  }

  render() {
    console.log('Books:', this.props.books.length)
    return (
      <div className="content">
          <section className="chart">
            <h1>Reading Chart</h1>
            <div>
              <label htmlFor="book" >Book:</label>
              {this.renderSelect()}
            </div>
            {this.renderChart()}
          </section>
      </div>
    )
  }
}
