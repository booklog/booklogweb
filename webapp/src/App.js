import React, { Component } from 'react';
import Header from './components/Header'
import Chart from './components/Chart'
import Login from './components/Login'
import API from './components/API'

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      selectedBook: '',
      books: [],
      read: [],
    }

    var p = this.getPath()
    this.dataForRoute(p)
  }

  getPath() {
    var pp = window.location.pathname.split('/')
    if (pp.length > 0) {
      return pp[1]
    } else {
      return null
    }
  }

  getBook = (b) => {
    API.bookLog(b).then(response => {
      this.setState({selectedBook: b, read: response.data.records.read})
    })
  }

  handleChange = (book) => {
    this.getBook(book)
  }

  handleClick = (chapter) => {
    API.bookLogUpdate('add', this.state.selectedBook,  chapter).then(response => {
      this.getBook(this.state.selectedBook)
    })
  }

  getBooks() {
    API.bookList().then(response => {
      let selected = response.data.records.length ? response.data.records[0].code : ''
      if (this.state.selectedBook === '') {
        this.getBook(selected)
      }
      this.setState({books: response.data.records, selectedBook: selected})
    })
  }

  dataForRoute(p) {
    if(p==='chart') {this.getBooks()}
  }

  renderChart(p) {
    if (!p) return
    return <Chart books={this.state.books} selectedBook={this.state.selectedBook} read={this.state.read} onChange={this.handleChange} onClick={this.handleClick} />
  }

  render() {
    var p = this.getPath()

    API.login("john", "secret")

    return (
      <div className="App">
        <Header />

        {!p ? <Login />: ''}

        {this.renderChart(p)}
      </div>
    );
  }
}

export default App;
