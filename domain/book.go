package domain

// Book holds the details of a book
type Book struct {
	Code     string `json:"code"`
	Name     string `json:"name"`
	Chapters int    `json:"chapters"`
}

// BookLog holds the details of a book and the chapters read
type BookLog struct {
	Book string `json:"book"`
	Name string `json:"name"`
	Read []int  `json:"read"`
}
