module gitlab.com/booklog/booklogweb

go 1.12

require (
	github.com/golang/protobuf v1.3.1
	github.com/gorilla/mux v1.7.0
	github.com/stretchr/testify v1.3.0
	gitlab.com/booklog/booklog v0.0.0-20190323005926-f3fecbaceba2
	gitlab.com/booklog/identity v0.0.0-20190316221406-5385940ba31b
	gitlab.com/booklog/router v0.0.0-20190309172531-8b8aef219c4f
	golang.org/x/lint v0.0.0-20190313153728-d0100b6bd8b3 // indirect
	golang.org/x/net v0.0.0-20190328230028-74de082e2cca
	golang.org/x/sys v0.0.0-20190329044733-9eb1bfa1ce65
	golang.org/x/text v0.3.0
	google.golang.org/genproto v0.0.0-20190327125643-d831d65fe17d
	google.golang.org/grpc v1.19.0
)
